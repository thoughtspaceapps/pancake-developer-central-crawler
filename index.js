(function(){

  /**
   * A basic web crawler meant for pulling sales data from the Pancake Developer central.
   * Credentials and plugin ID should be set in config.json prior to running the crawler.
   * Plugin ID is the randomized string found at the end of the your plugin's URL in the Pancake store, like OP6xMf8U
  **/

  var settings = require('./config.json');
  settings.devCentralUrl = 'http://manage.pancakeapp.com/';


  var cheerio = require('cheerio');
  var nodemailer = require('nodemailer');
  var Q = require('q');
  if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage(__dirname+'/localStorage');
  }
  var request = require('request').defaults(
    {
      "followAllRedirects":true,
      "jar": true,
    }
  );



  function login(){
    var deferred = Q.defer();
    var loginOptions = {
      "form":{
        "username": settings.username,
        "password": settings.password
      }
    };

    request.post(settings.devCentralUrl+'plugins/login', loginOptions, function(error, response, body){
      if (!error && response.statusCode == 200 && body.indexOf("You have logged in successfully.") != -1) {
        console.log("Successfully logged in!");
        deferred.resolve(true);
      }
      else{
        console.log("Issue logging in, please check config.json");
        deferred.resolve(false);
      }
    });

    return deferred.promise;

  }


  /**
   * function getSales(timePeriod)
   *
   * This function checks the total sales that have been made during the specified time period
   * timePeriod is either "week", "month", "year", or "all"
  **/

  function getSales(timePeriod){
    var deferred = Q.defer();
    request.get(settings.devCentralUrl+"plugins/view/"+settings.pluginId, {}, function(error, response, body){
      if (!error && response.statusCode == 200 && body.indexOf("Statistics") != -1) {
        $ = cheerio.load(body);
        var saleInfo = {};
        if(timePeriod == "week"){
          saleInfo['count'] = $('main>.row:first-of-type>.col-md-3:first-of-type .list-group>.list-group-item:first-of-type>span').text();
          saleInfo['amount'] = $('main>.row:first-of-type>.col-md-3:first-of-type .list-group>.list-group-item:last-of-type>span').text();
        }
        else if(timePeriod == "month"){
          saleInfo['count'] = $('main>.row:first-of-type>.col-md-3:nth-child(2) .list-group>.list-group-item:first-of-type>span').text();
          saleInfo['amount'] = $('main>.row:first-of-type>.col-md-3:nth-child(2) .list-group>.list-group-item:last-of-type>span').text();
        }
        else if(timePeriod == "year"){
          saleInfo['count'] = $('main>.row:first-of-type>.col-md-3:nth-child(3) .list-group>.list-group-item:first-of-type>span').text();
          saleInfo['amount'] = $('main>.row:first-of-type>.col-md-3:nth-child(3) .list-group>.list-group-item:last-of-type>span').text();
        }
        else if(timePeriod == "all"){
          saleInfo['count'] = $('main>.row:first-of-type>.col-md-3:nth-child(4) .list-group>.list-group-item:first-of-type>span').text();
          saleInfo['amount'] = $('main>.row:first-of-type>.col-md-3:nth-child(4) .list-group>.list-group-item:last-of-type>span').text();
        }
        else{
          console.log('please specify a time period to check sales for.');
          deferred.resolve(false);
        }
        deferred.resolve(saleInfo);
      }
      else{
        console.log("Issue getting sales, please check config.json");
        deferred.resolve(false);
      }
    });
    return deferred.promise;
  }

  /**
   * function checkForNewSales()
   *
   * This function checks for any new sales and sends an email if it finds any.
  **/
  function checkForNewSales(){
    login().then(function(didLogIn){
      if(didLogIn){
        getSales('all').then(function(salesInfo){
          if(salesInfo){
            if(localStorage.getItem('salesInfo') != null){
              var oldSalesInfo = JSON.parse(localStorage.getItem('salesInfo'));
              if(oldSalesInfo.count < salesInfo.count){
                console.log('New sale made!');

                //Send an email to let the user know there was a new sale.
                var transporter = nodemailer.createTransport(settings.smtpSettings);
                transporter.sendMail({
                  from: settings.smtpSettings.auth.user,
                  to: settings.notificationEmail,
                  subject: 'New Sale for '+settings.pluginName+'!',
                  text: 'Your plugin '+settings.pluginName+' received a new sale. Check out your Pancake developer central account for more.'
                }, function(error, info){
                  if(error != null){
                    console.log(error);
                  }
                });

                //Update our sales info in localStorage
                localStorage.setItem('salesInfo', JSON.stringify(salesInfo));
              }
              else{
                console.log('No new sales');
              }
            }
            else{
              localStorage.setItem('salesInfo', JSON.stringify(salesInfo));
              console.log('First run, storing current sale info for later checks.');
            }
          }
        });
      }
      else{
        console.log('Ending script due to log in issue');
      }
    });
  }


  checkForNewSales();



}());
