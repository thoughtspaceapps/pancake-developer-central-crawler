# Pancake Developer Central Crawler
---
This tool crawls the pancake developer central site for the specified plugin ID, and determines if a new sale has been made. If a sale has been made, the script sends a notification email to the address specified in the config.json file.

The script works by scraping the dev central and looking up all time sales count. After the first check, the script stores the sales data locally for comparison on the next run. If the sales count goes up, an email is sent.

## Installation
To install this script, simply clone the repository down to your computer. You must also have Node.JS installed on your system for this script to work. You can either run the script manually via `node .`, or you can set it up on a cron using `crontab -e` in terminal. Below is a sample cron configuration that runs a check every ten minutes:

`*/10 * * * * /usr/local/bin/node /Users/YOUR_USER/pancake-dev-central-crawler/index.js`
